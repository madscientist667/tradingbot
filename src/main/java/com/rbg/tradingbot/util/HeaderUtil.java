package com.rbg.tradingbot.util;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.socket.WebSocketHttpHeaders;

public final class HeaderUtil {

	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String ACCEPT_LANGUAGE_HEADER = "Accept-Language";
	private static final String ACCEPT_LANGUAGE = "nl-NL,en;q=0.8";
	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static final String APPLICATION_JSON = "application/json";
	private static final String ACCEPT_HEADER = "Accept";

	public static WebSocketHttpHeaders generateWebsocketHeaders(String token) {
		WebSocketHttpHeaders handshakeHeaders = new WebSocketHttpHeaders();
		handshakeHeaders.add(AUTHORIZATION_HEADER, token);
		handshakeHeaders.add(ACCEPT_LANGUAGE_HEADER, ACCEPT_LANGUAGE);
		return handshakeHeaders;
	}

	public static MultiValueMap<String, String> generateHTTPHeaders(String token) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap();
		headers.add(CONTENT_TYPE_HEADER, APPLICATION_JSON);
		headers.add(ACCEPT_HEADER, APPLICATION_JSON);
		headers.add(AUTHORIZATION_HEADER, token);
		headers.add(ACCEPT_LANGUAGE_HEADER, ACCEPT_LANGUAGE);
		return headers;
	}
}
