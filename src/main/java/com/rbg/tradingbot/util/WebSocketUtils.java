package com.rbg.tradingbot.util;

import com.google.gson.Gson;
import com.rbg.tradingbot.interfaces.MessageProcessor;
import com.rbg.tradingbot.interfaces.WebSocketMessage;
import com.rbg.tradingbot.service.MyWebsocketHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;

import static com.rbg.tradingbot.util.HeaderUtil.generateWebsocketHeaders;

public final class WebSocketUtils {

	private static final Logger logger = LogManager.getLogger(WebSocketUtils.class);

	private static final Gson gson = new Gson();
	private static WebSocketSession session;

	/**
	 * Opens websocket connection and waits for the session result
	 *
	 * @param messageProcessor  - custom message processor
	 * @param url               - url for websocket connection
	 * @param token             - authentication user token
	 */
	public static void openConnectionToWebsocketServer(MessageProcessor messageProcessor, String url, String token) {
		if (session == null) {
			WebSocketClient client = new StandardWebSocketClient();

			ListenableFuture<WebSocketSession> future = client.doHandshake(
					new MyWebsocketHandler(messageProcessor), generateWebsocketHeaders(token), URI.create(url));

			try {
				session = future.get();
			} catch (InterruptedException | ExecutionException e) {
				logger.error("Error on opening websocket connection: " + e.getMessage());
			}
		}
	}

	/**
	 * Sends websocket message
	 * @param message - websocket message
	 */
	public static void sendMessage(WebSocketMessage message) {
		try {
			TextMessage textMessage = new TextMessage(gson.toJson(message));
			session.sendMessage(textMessage);
		} catch (IOException e) {
			logger.error("Error on sending websocket message: " + e.getMessage());
		}
	}
}
