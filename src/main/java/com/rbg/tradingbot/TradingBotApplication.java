package com.rbg.tradingbot;

import com.rbg.tradingbot.interfaces.MessageProcessor;
import com.rbg.tradingbot.service.BotService;
import com.rbg.tradingbot.service.TradingQuoteMessageProcessor;
import com.rbg.tradingbot.service.TradingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Scanner;

@SpringBootApplication
public class TradingBotApplication {

	private static String productId;
	private static double buyPrice;
	private static double upperLimitPrice;
	private static double lowerLimitPrice;
	private static double investingAmount;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter Product ID: ");
		productId = scanner.next();

		System.out.print("Enter buy price: ");
		buyPrice = scanner.nextDouble();

		System.out.print("Enter upper limit price: ");
		upperLimitPrice = scanner.nextDouble();

		System.out.print("Enter lower limit price: ");
		lowerLimitPrice = scanner.nextDouble();

		System.out.print("Enter investing amount: ");
		investingAmount = scanner.nextDouble();

		ApplicationContext applicationContext = SpringApplication.run(TradingBotApplication.class, args);

		BotService service = applicationContext.getBean(BotService.class);
		service.startBot(productId);
	}

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Value("${open.trade.url}")
	private String buyUrl;
	@Value("${close.trade.url}")
	private String sellUrl;
	@Value("${user.token}")
	private String token;

	@Bean
	MessageProcessor createMessageProcessor() {
		return new TradingQuoteMessageProcessor(productId, buyPrice, upperLimitPrice, lowerLimitPrice, investingAmount,
				new TradingService(buyUrl, sellUrl, token, new RestTemplate()));
	}
}
