package com.rbg.tradingbot.service;

import com.rbg.tradingbot.interfaces.MessageProcessor;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class MyWebsocketHandler extends TextWebSocketHandler {

	private static boolean doneProcessing = true;

	private final MessageProcessor messageProcessor;

	public MyWebsocketHandler(MessageProcessor messageProcessor) {
		this.messageProcessor = messageProcessor;
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) {
		if (doneProcessing) {
			doneProcessing = false;
			doneProcessing = messageProcessor.processMessage(message.getPayload());
		}
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}
}