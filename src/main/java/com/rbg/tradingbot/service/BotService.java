package com.rbg.tradingbot.service;

import com.rbg.tradingbot.interfaces.MessageProcessor;
import com.rbg.tradingbot.messages.SubscribeMessage;
import com.rbg.tradingbot.util.WebSocketUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BotService {

	public static final String TRADING_PRODUCT = "trading.product.";

	@Value("${websocket.url}")
	private String websocketUrl;
	@Value("${user.token}")
	private String token;

	private final MessageProcessor messageProcessor;

	public BotService(MessageProcessor messageProcessor) {
		this.messageProcessor = messageProcessor;
	}

	public void startBot(String productId) {
		WebSocketUtils.openConnectionToWebsocketServer(messageProcessor, websocketUrl, token);
		SubscribeMessage subscribeMessage = new SubscribeMessage();
		subscribeMessage.addSubscription(TRADING_PRODUCT + productId);
		WebSocketUtils.sendMessage(subscribeMessage);
	}
}
