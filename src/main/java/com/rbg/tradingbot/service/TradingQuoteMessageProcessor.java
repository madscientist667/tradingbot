package com.rbg.tradingbot.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rbg.tradingbot.dto.TradingQuoteDto;
import com.rbg.tradingbot.dto.enums.TradeDirection;
import com.rbg.tradingbot.exception.ClosePositionException;
import com.rbg.tradingbot.exception.OpenPositionException;
import com.rbg.tradingbot.interfaces.MessageProcessor;
import com.rbg.tradingbot.interfaces.TradingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class TradingQuoteMessageProcessor implements MessageProcessor {

	private static final Logger logger = LogManager.getLogger(MyWebsocketHandler.class);

	public static final String EXCEPTION_WRONG_PRICES = "Wrong prices. It needs to be: lower limit price < buy price < upper limit price";
	private static final String TRADING_QUOTE_PROPERTY = "trading.quote";
	private static final String BODY_PROPERTY = "body";

	private static final Map<String, TradeDirection> tradingEventCache = new HashMap<>();

	private final String productId;
	private final double buyPrice;
	private final double upperLimitPrice;
	private final double lowerLimitPrice;
	private final double investingAmount;
	private final TradingEvent tradingEvent;

	private void validateInput(double buyPrice, double upperLimitPrice, double lowerLimitPrice) {
		if (!(lowerLimitPrice < buyPrice && buyPrice < upperLimitPrice)) {
			logger.error(EXCEPTION_WRONG_PRICES);
			throw new IllegalArgumentException(EXCEPTION_WRONG_PRICES);
		}
	}

	public TradingQuoteMessageProcessor(String productId, double buyPrice, double upperLimitPrice, double lowerLimitPrice, double investingAmount, TradingEvent tradingEvent) {
		this.productId = productId;
		this.buyPrice = buyPrice;
		this.upperLimitPrice = upperLimitPrice;
		this.lowerLimitPrice = lowerLimitPrice;
		this.investingAmount = investingAmount;
		this.tradingEvent = tradingEvent;
		validateInput(buyPrice, upperLimitPrice, lowerLimitPrice);
	}

	@Override
	public boolean processMessage(String message) {
		try {
			if (message.contains(TRADING_QUOTE_PROPERTY)) {
				TradingQuoteDto dto = parseMessage(message);
				if (this.productId.equals(dto.getSecurityId())) {
					double currentPrice = dto.getCurrentPrice();
					logger.info("currentPrice: " + currentPrice);
					if (currentPrice <= buyPrice && tradingEventCache.get(productId) == null) {
						tradingEventCache.put(productId, TradeDirection.BUY);
						tradingEvent.triggerTradeEvent(TradeDirection.BUY, productId, investingAmount);
					} else if ((currentPrice >= upperLimitPrice || currentPrice <= lowerLimitPrice) && tradingEventCache.get(productId) != null) {
						tradingEventCache.remove(productId);
						tradingEvent.triggerTradeEvent(TradeDirection.SELL, productId, investingAmount);
					}
				}
			} else {
				logger.info(message);
			}
		} catch (ClosePositionException e) {
			tradingEventCache.put(productId, TradeDirection.BUY);
		} catch (OpenPositionException e) {
			tradingEventCache.remove(productId);
		} catch (Exception e) {
			logger.error("Error on processing websocket message: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage()));
		}
		return true;
	}

	private TradingQuoteDto parseMessage(String message) {
		Gson g = new Gson();
		JsonObject messageJson = JsonParser.parseString(message).getAsJsonObject();
		JsonObject body = messageJson.get(BODY_PROPERTY).getAsJsonObject();
		return g.fromJson(body, TradingQuoteDto.class);
	}
}