package com.rbg.tradingbot.service;

import com.rbg.tradingbot.dto.ClosePositionResponse;
import com.rbg.tradingbot.dto.InvestingAmountDto;
import com.rbg.tradingbot.dto.OpenPositionRequest;
import com.rbg.tradingbot.dto.OpenPositionResponse;
import com.rbg.tradingbot.dto.enums.TradeDirection;
import com.rbg.tradingbot.exception.ClosePositionException;
import com.rbg.tradingbot.exception.OpenPositionException;
import com.rbg.tradingbot.interfaces.TradingEvent;
import com.rbg.tradingbot.messages.UnsubscribeMessage;
import com.rbg.tradingbot.util.WebSocketUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

import static com.rbg.tradingbot.service.BotService.TRADING_PRODUCT;
import static com.rbg.tradingbot.util.HeaderUtil.generateHTTPHeaders;

public class TradingService implements TradingEvent {

	private static final Logger logger = LogManager.getLogger(MyWebsocketHandler.class);

	private static final Map<String, OpenPositionResponse> tradeCache = new HashMap<>();

	private final String buyUrl;
	private final String sellUrl;
	private final String token;
	private final RestTemplate restTemplate;

	public TradingService(String buyUrl, String sellUrl, String token, RestTemplate restTemplate) {
		this.buyUrl = buyUrl;
		this.sellUrl = sellUrl;
		this.token = token;
		this.restTemplate = restTemplate;
	}

	@Override
	public void triggerTradeEvent(TradeDirection tradeDirection, String productId, double investingAmount) throws OpenPositionException, ClosePositionException {
		if (tradeDirection.equals(TradeDirection.BUY)) {
			openPosition(productId, investingAmount);
		} else {
			closePosition(productId);
			unsubscribe(productId);
		}
	}

	private void closePosition(String productId) throws ClosePositionException {
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(sellUrl);
			String uri = String.format(builder.toUriString(), tradeCache.get(productId).getPositionId());

			HttpEntity<Object> requestEntity = new HttpEntity<>(null, generateHTTPHeaders(token));

			ResponseEntity<ClosePositionResponse> response =
					restTemplate.exchange(uri, HttpMethod.GET, requestEntity, ClosePositionResponse.class);
			HttpStatus statusCode = response.getStatusCode();

			if (statusCode.is2xxSuccessful()) {
				tradeCache.remove(productId);
				logger.info(response.getBody());
			} else {
				logger.info("Something went wrong on closing position: " + statusCode.getReasonPhrase());
				throw new ClosePositionException();
			}
		} catch (Exception e) {
			logger.error("Error on closing position: " + e.getMessage());
			throw new ClosePositionException();
		}
	}

	private void openPosition(String productId, double investingAmount) throws OpenPositionException {
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(buyUrl);

			OpenPositionRequest request = getOpenPositionRequest(productId, investingAmount);
			HttpEntity<Object> requestEntity = new HttpEntity<>(request, generateHTTPHeaders(token));

			ResponseEntity<OpenPositionResponse> response = restTemplate.exchange(
					builder.toUriString(), HttpMethod.POST, requestEntity, OpenPositionResponse.class);
			HttpStatus statusCode = response.getStatusCode();

			if (statusCode.is2xxSuccessful()) {
				tradeCache.put(productId, response.getBody());
				logger.info(response.getBody());
			} else {
				logger.info("Something went wrong on opening position: " + statusCode.getReasonPhrase());
				throw new OpenPositionException();
			}
		} catch (Exception e) {
			logger.error("Error on opening position: " + e.getMessage());
			throw new OpenPositionException();
		}
	}

	private OpenPositionRequest getOpenPositionRequest(String productId, double investingAmount) {
		OpenPositionRequest request = new OpenPositionRequest();
		request.setProductId(productId);
		request.setLeverage(1);
		request.setDirection(TradeDirection.BUY.name());
		InvestingAmountDto investingAmountDto = new InvestingAmountDto();
		investingAmountDto.setAmount(String.valueOf(investingAmount));
		investingAmountDto.setCurrency("BUX");
		investingAmountDto.setDecimals(2);
		request.setInvestingAmount(investingAmountDto);
		return request;
	}

	private void unsubscribe(String productId) {
		UnsubscribeMessage unsubscribeMessage = new UnsubscribeMessage();
		unsubscribeMessage.unsubscribeFrom(TRADING_PRODUCT + productId);
		WebSocketUtils.sendMessage(unsubscribeMessage);
	}
}
