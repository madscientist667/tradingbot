package com.rbg.tradingbot.messages;

import com.rbg.tradingbot.interfaces.WebSocketMessage;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UnsubscribeMessage implements WebSocketMessage {
	private List<String> unsubscribeFrom;

	public UnsubscribeMessage() {
		this.unsubscribeFrom = new ArrayList<>();
	}

	public void unsubscribeFrom(String productId) {
		unsubscribeFrom.add(productId);
	}
}
