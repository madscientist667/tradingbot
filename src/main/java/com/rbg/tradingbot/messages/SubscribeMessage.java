package com.rbg.tradingbot.messages;

import com.rbg.tradingbot.interfaces.WebSocketMessage;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SubscribeMessage implements WebSocketMessage {
	private List<String> subscribeTo;

	public SubscribeMessage() {
		this.subscribeTo = new ArrayList<>();
	}

	public void addSubscription(String productId) {
		subscribeTo.add(productId);
	}
}
