package com.rbg.tradingbot.interfaces;

import com.rbg.tradingbot.dto.enums.TradeDirection;
import com.rbg.tradingbot.exception.ClosePositionException;
import com.rbg.tradingbot.exception.OpenPositionException;

public interface TradingEvent {

	/**
	 * This will execute a HTTP call to open or close a position.
	 *
	 * @param tradeDirection    - BUY or SELL
	 * @param productId         - product id representing a specific stock
	 * @param investingAmount   - investing amount
	 * @throws OpenPositionException    - exception if something goes wrong when opening a position
	 * @throws ClosePositionException   - exception if something goes wrong when closing a position
	 */
	void triggerTradeEvent(TradeDirection tradeDirection, String productId, double investingAmount) throws OpenPositionException, ClosePositionException;
}
