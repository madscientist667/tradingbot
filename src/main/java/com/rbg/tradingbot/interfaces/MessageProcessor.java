package com.rbg.tradingbot.interfaces;

public interface MessageProcessor {

	/**
	 * Process websocket message if it's a trading quote otherwise it just logs it.
	 * If it's a trading quote based on current price and lower/upper limits it will call for a
	 * SELL/BUY event.
	 *
	 * @param message - message from the websocket
	 * @return true when processing is done
	 */
	boolean processMessage(String message);
}
