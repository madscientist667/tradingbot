package com.rbg.tradingbot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TradingQuoteDto {
	private String securityId;
	private String currentPrice;

	public Double getCurrentPrice() {
		return Double.valueOf(currentPrice);
	}
}
