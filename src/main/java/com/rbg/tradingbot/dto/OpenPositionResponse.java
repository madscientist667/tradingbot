package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class OpenPositionResponse {
	private String id;
	private String positionId;
	private ProductDto product;
	private InvestingAmountDto investingAmount;
	private PriceDto price;
	private Integer leverage;
	private String direction;
	private String type;
	private Long dateCreated;
}
