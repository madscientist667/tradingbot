package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class ClosePositionResponse {
	private String id;
	private String positionId;
	private PriceDto profitAndLoss;
	private ProductDto product;
	private PriceDto investingAmount;
	private PriceDto price;
	private Integer leverage;
	private String direction;
	private String type;
	private Long dateCreated;
}
