package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class PriceDto {
	private String currency;
	private Integer decimals;
	private String amount;
}
