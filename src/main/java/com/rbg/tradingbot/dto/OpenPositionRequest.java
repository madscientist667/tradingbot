package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class OpenPositionRequest {
	private String productId;
	private InvestingAmountDto investingAmount;
	private Integer leverage;
	private String direction;
}
