package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class ProductDto {
	private String securityId;
	private String symbol;
	private String displayName;
}
