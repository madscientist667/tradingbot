package com.rbg.tradingbot.dto;

import lombok.Data;

@Data
public class InvestingAmountDto {
	private String currency;
	private Integer decimals;
	private String amount;
}
