package com.rbg.tradingbot;

import com.rbg.tradingbot.dto.enums.TradeDirection;
import com.rbg.tradingbot.exception.ClosePositionException;
import com.rbg.tradingbot.exception.OpenPositionException;
import com.rbg.tradingbot.interfaces.TradingEvent;
import com.rbg.tradingbot.service.TradingQuoteMessageProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.lang.reflect.Field;
import java.util.Map;

import static com.rbg.tradingbot.service.TradingQuoteMessageProcessor.EXCEPTION_WRONG_PRICES;

@RunWith(MockitoJUnitRunner.class)
public class TradingQuoteMessageProcessorTest {

	private static final String PRODUCT_ID = "sb26493";
	private static final int BUY_PRICE = 100;
	private static final int UPPER_LIMIT_PRICE = 110;
	private static final int LOWER_LIMIT_PRICE = 90;
	private static final int INVESTING_AMOUNT = 1000;
	private static final String OTHER_MESSAGE = "test";
	private static final String CORRECT_JSON = "{\"t\":\"trading.quote\",\"id\":\"98519502-d508-11eb-be87-879c5d9fa999\",\"v\":2,\"body\":{\"securityId\":\"sb26493\",\"currentPrice\":\"1.19307\"}}";
	private static final String WRONG_JSON = "{\"t\":\"trading.quote\",\"id\":\"98519502-d508-11eb-be87-879c5d9fa999\",\"v\":2,\"bodyWrong\":{\"securityId\":\"sb26493\",\"currentPrice\":\"1.19307\"}}";
	private static final String PARTIAL_JSON = "{\"t\":\"trading.quote\",\"body\":{\"securityId\":\"sb26493\",\"currentPrice\":\"1.19307\"}}";
	private static final String WRONG_PRODUCT_ID = "{\"t\":\"trading.quote\",\"id\":\"98519502-d508-11eb-be87-879c5d9fa999\",\"v\":2,\"bodyWrong\":{\"securityId\":\"sb33333\",\"currentPrice\":\"1.19307\"}}";

	@Rule
	public MockitoRule rule = MockitoJUnit.rule();
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Mock
	private TradingEvent tradingEvent;

	private TradingQuoteMessageProcessor tradingQuoteMessageProcessor;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		tradingQuoteMessageProcessor = new TradingQuoteMessageProcessor(PRODUCT_ID, BUY_PRICE, UPPER_LIMIT_PRICE, LOWER_LIMIT_PRICE, 1000, tradingEvent);
	}

	@Test
	public void testValidateInputException1() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(EXCEPTION_WRONG_PRICES);

		new TradingQuoteMessageProcessor(PRODUCT_ID, BUY_PRICE, BUY_PRICE, UPPER_LIMIT_PRICE, INVESTING_AMOUNT, tradingEvent);
	}

	@Test
	public void testValidateInputException2() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(EXCEPTION_WRONG_PRICES);

		new TradingQuoteMessageProcessor(PRODUCT_ID, BUY_PRICE, LOWER_LIMIT_PRICE, LOWER_LIMIT_PRICE, INVESTING_AMOUNT, tradingEvent);
	}

	@Test
	public void testValidateInput() {
		new TradingQuoteMessageProcessor(PRODUCT_ID, BUY_PRICE, UPPER_LIMIT_PRICE, LOWER_LIMIT_PRICE, INVESTING_AMOUNT, tradingEvent);
	}

	@Test
	public void testProcessMessageOtherMessage() {
		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(OTHER_MESSAGE);
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(0, getPrivateFieldCacheReflection().size());
	}

	@Test
	public void testProcessMessageWrongJson() {
		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(WRONG_JSON);
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(0, getPrivateFieldCacheReflection().size());
	}

	@Test
	public void testProcessMessageWrongProductId() {
		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(WRONG_PRODUCT_ID);
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(0, getPrivateFieldCacheReflection().size());
	}

	@Test
	public void testProcessMessagePartialJson() {
		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(PARTIAL_JSON);
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(1, getPrivateFieldCacheReflection().size());
		cleanupCache();
	}

	@Test
	public void testProcessMessage() {
		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(CORRECT_JSON);
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(1, getPrivateFieldCacheReflection().size());
		cleanupCache();
	}

	@Test
	public void testProcessMessageThrowOpenPositionExp() throws OpenPositionException, ClosePositionException {
		Mockito.doThrow(new OpenPositionException()).when(tradingEvent).triggerTradeEvent(Mockito.any(), Mockito.any(), Mockito.anyDouble());

		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(CORRECT_JSON); //buy with error
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(0, getPrivateFieldCacheReflection().size());
	}

	@Test
	public void testProcessMessageThrowClosePositionExp() throws OpenPositionException, ClosePositionException {
		Mockito.doThrow(new ClosePositionException()).when(tradingEvent).triggerTradeEvent(Mockito.any(), Mockito.any(), Mockito.anyDouble());

		boolean doneProcessing = this.tradingQuoteMessageProcessor.processMessage(CORRECT_JSON); //buy
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(1, getPrivateFieldCacheReflection().size());

		doneProcessing = this.tradingQuoteMessageProcessor.processMessage(CORRECT_JSON); //sell with error
		Assert.assertTrue(doneProcessing);
		Assert.assertNotNull(getPrivateFieldCacheReflection());
		Assert.assertEquals(1, getPrivateFieldCacheReflection().size());
		cleanupCache();
	}

	private void cleanupCache() {
		getPrivateFieldCacheReflection().remove(PRODUCT_ID);
	}

	private Map<String, TradeDirection> getPrivateFieldCacheReflection() {
		Class<?> secretClass = this.tradingQuoteMessageProcessor.getClass();
		for (Field field : secretClass.getDeclaredFields()) {
			System.out.println("Field Name: " + field.getName());
			if (field.getName().equals("tradingEventCache")) {
				field.setAccessible(true);
				try {
					return (Map<String, TradeDirection>) field.get(this.tradingQuoteMessageProcessor);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
